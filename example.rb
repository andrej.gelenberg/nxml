require_relative 'lib/nxml'

doc = NXML.new do
  self.version = "1.0"
  self.standalone = true
  self.encoding = Encoding::UTF_8

  doctype.pubref "test", "foo"

  root "root_tag_name" do
    self.attribute_name = "value"

    attribute "attribute_name_2", "value 2"

    a "attribute_name_3", "value 3"

    tag "child1" # <child1/>

    tag "child2" do # <child2></child2>
      text ""
    end

    tag "child3" do
      cdata "cdata <foo></bla>"
    end

    t = tag "child3"
    t.text "test foo"

  end
end

puts doc.to_str
