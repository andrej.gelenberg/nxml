Gem::Specification.new do |s|
  s.name = "NXML"
  s.version = "0.0.1"
  s.date = "2019-05-27"
  s.summary = "NXML"
  s.description = "library for generating XML 1.0 documents"
  s.authors = ["Andrej Gelenberg"]
  s.email = "andrej.gelenberg@udo.edu"
  s.homepage = 'https://gitlab.com/andrej.gelenberg/nxml'
  s.license = 'MIT'
  s.files = ['lib/nxml.rb']
end
