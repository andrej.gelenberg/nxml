# Andrej Gelenberg <andrej.gelenberg@udo.edu> (c) 2019

class NXML

  NAME_START = ":A-Z_a-z\u{C0}-\u{D6}\u{D8}-\u{F6}\u{F8}-\u{2FF}\u{370}-\u{37D}\u{37F}-\u{1FFF}\u{200C}-\u{200D}\u{2070}-\u{218F}\u{2C00}-\u{2FEF}\u{3001}-\u{D7FF}\u{F900}-\u{FDCF}\u{FDF0}-\u{FFFD}\u{10000}-\u{EFFFF}"
  NAME_CHAR = "\\-.0-9\u{B7}\u{0300}-\u{036F}\u{203F}-\u{2040}"

  NAME_RE  = Regexp.new "^[#{NAME_START}][#{NAME_START}#{NAME_CHAR}]*$"
  PUBID_RE = Regexp.new "^[\u{20}\u{D}\u{A}a-zA-Z0-9'()+,./:=?;!*#@$_%]*$"

  ATTR_ESCAPE_HASH = {
    '&' => '&amp;',
    '"' => '&quot;',
    '<' => '&lt;'
  }
  ATTR_ESCAPE = Regexp.new "[#{ATTR_ESCAPE_HASH.keys.join}]"

  TEXT_ESCAPE_HASH = {
    '&' => '&amp;',
    '<' => '&gt;',
    '>' => '&lt;'
  }
  TEXT_ESCAPE = Regexp.new "[#{TEXT_ESCAPE_HASH.keys.join}]"

  def self.check_name(name)
    if not name.match? NAME_RE
      raise ArgumentError.new "invalid XML name \"#{name}\""
    end
  end

  def self.check_pubid(pubid)
    if not name.match? PUBID_RE
      raise ArgumentError.new "invalid public id \"#{pubid}\""
    end
  end

  def self.escape_attr_value(value)
    value.gsub ATTR_ESCAPE, ATTR_ESCAPE_HASH
  end

  def self.escape_attr_raw(value)
    value.gsub /["]/, '&quot;'
  end

  def self.escape_text(value)
    value.gsub TEXT_ESCAPE, TEXT_ESCAPE_HASH
  end

  def self.escape_text_raw(value)
    value.gsub /[<]/, '&lt;'
  end

  class Text
    def initialize(text, raw = false)
      if raw
        @text = NXML::escape_text_raw text
      else
        @text = NXML::escape_text text
      end
    end

    def to_str
      @text
    end
  end

  class Element
    def initialize(name, &block)
      NXML::check_name name

      @name       = name
      @attributes = {}
      @children   = []

      instance_eval &block if block_given?
    end

    def method_missing(name, value = nil, *attrs)
      if name[-1] == '='
        attribute name[0..-2], value
      else
        super name, value, *attrs
      end
    end

    def get_tag_name()
      @name
    end

    def tag(name, &block)
      r = Element.new(name, &block)
      @children << r
      r
    end

    def text(text)
      r = Text.new(text)
      @children << r
      r
    end

    def cdata(text)
      r = CDATA.new(text)
      @children << r
      r
    end

    def attribute(name, value = nil)
      NXML::check_name name
      r = if value.is_a? Symbol then value else NXML::escape_attr_value value end
      @attributes[name] = r
      r
    end

    def attribute_raw(name, value)
      XML::check_name name

      r = NXML::escape_attr_raw value
      @attributes[name] = r
      r
    end

    def a(name, value = nil)
      attribute name, value
    end

    def to_str
      ret = "<#{@name}"

      if not @attributes.empty?
        for k,v in @attributes
          ret += " #{k}=\"#{v}\""
        end
      end

      if @children.empty?
        ret += "/>"
      else
        ret += ">#{@children.join}</#{@name}>"
      end

      ret
    end

  end

  class CDATA
    def initialize(data)
      if not data.index(']]>').nil?
        raise ArgumentError.new "CDATA should not contain CDATA closing sequence ']]>'"
      end

      @data = data
    end

    def to_str
      "<![CDATA[#{@data}]]>"
    end
  end

  class ExternalID
    def initialize(id, sysid = nil)
      if not sysid.nil?
        NXML::check_pubid id
        @pubid = id
      else
        sysid = id
      end

      if not sysid.include? '"'
        @sysid = "\"#{sysid}\""
      elsif sysid.include? "'"
        raise ArgumentError.new "System id should not contain ' and \" at the same time"
      else
        @sysid = "'#{sysid}'"
      end

    end

    def to_str
      if not @pubid.nil?
        "PUBLIC \"#{@pubid}\" #{@sysid}"
      else
        "SYSTEM #{@sysid}"
      end
    end
  end

  class Doctype
    def initialize(doc, &block)
      @doc = doc
      instance_eval &block if block_given?
    end

    def pubref(pubid, sysid)
      @extid = ExternalID.new pubid, sysid
    end

    def system(sysid)
      @extid = ExternalID.new sysid
    end

    def to_str()
      r = "<!DOCTYPE #{@doc.get_root_tag_name}"

      if not @extid.nil?
        r += " #{@extid.to_str}"
      end

      r += ">"
    end
  end

  attr_accessor :version, :encoding, :standalone, :root

  def initialize(&block)
    @version = "1.0"
    @encoding = Encoding::UTF_8

    instance_eval &block if block_given?
  end

  def get_root_tag_name()
    @root.get_tag_name
  end

  def root(tag, &block)
    if tag.is_a? Element
      @root = tag
    else
      @root = Element.new tag, &block
    end
  end

  def doctype(&block)
    @doctype = Doctype.new self, &block
  end

  def to_str
    ret = "<?xml"

    if not @version.nil?
      ret += " version=\"#{@version}\""
    end

    if not @encoding.nil?
      ret += " encoding=\"#{@encoding.name}\""
    end

    if not @standalone.nil?
      ret += " standalone=\"#{@standalone ? "yes" : "no"}\""
    end

    ret += "?>\n"

    if not @doctype.nil?
      ret += "#{@doctype.to_str}\n"
    end

    ret += @root

    ret.encode @encoding
  end
end
